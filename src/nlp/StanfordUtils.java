/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nlp;

import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nandadeep
 * @Description: Named Entity Recognizer
 */
public class StanfordUtils {
    
    public static LinkedHashMap<String, ArrayList<String>> identifyNER(String text, String model) {
        LinkedHashMap<String, ArrayList<String>> map = new <String, ArrayList<String>>LinkedHashMap();
        String serializedClassifier = model;
        System.out.println(serializedClassifier);
        CRFClassifier<CoreLabel> classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
        List<List<CoreLabel>> classify = classifier.classify(text);
        for (List<CoreLabel> coreLabels : classify) {
            for (CoreLabel coreLabel : coreLabels) {

                String word = coreLabel.word();
                String category = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
                if (!"O".equals(category)) {
                    if (map.containsKey(category)) {
                        // key is already their just insert in arraylist
                        map.get(category).add(word);
                    } else {
                        ArrayList<String> temp = new ArrayList<>();
                        temp.add(word);
                        map.put(category, temp);
                    }
                    //System.out.println(word + ":" + category);
                }

            }

        }
        return map;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        //demo
        String content = "Sachin Ramesh Tendulkar (Listeni/ˌsətʃɪn tɛnˈduːlkər/; Marathi: "
                + " सचिन रमेश तेंडुलकर; born 24 April 1973) is an Indian former cricketer widely "
                + " acknowledged as the greatest batsman of the modern generation, popularly holds the title \"God of Cricket\" among his fans [2] He is also acknowledged as the greatest cricketer of all time.[6][7][8][9] He took up cricket at the age of eleven, made his Test debut against Pakistan at the age of sixteen, and went on to represent Mumbai domestically and India internationally for close to twenty-four years. He is the only player to have scored one hundred international centuries, the first batsman to score a Double Century in a One Day International, and the only player to complete more than 30,000 runs in international cricket.[10] In October 2013, he became the 16th player and first Indian to aggregate "
                + " 50,000 runs in all recognized cricket "
                + " First-class, List A and Twenty20 combined)";
        int sum = 0;
        float score = 0;
        LinkedHashMap<String,ArrayList<String>> output = identifyNER(content, "/home/hadoop/NetBeansProjects/Privacy/stanford-ner-2014-01-04/classifiers/english.conll.4class.distsim.crf.ser.gz");
        for(Map.Entry<String,ArrayList<String>> entry : output.entrySet()) {
            if(entry.getKey().equalsIgnoreCase("person")) {
                score = score + entry.getValue().size() + 2; //revealing person info = private af.
            } else if(entry.getKey().equalsIgnoreCase("location")) {
                score = score + entry.getValue().size(); // revealing location info = not that private af.
            } else {
                score = (float)(score + entry.getValue().size() *0.5); //revealing misc info is scored but not that important.
            }
        }
        System.out.println(score);
    }
}
