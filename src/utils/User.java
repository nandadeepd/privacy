/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Nandadeep
 * @Description: User model class to store features to be used in vector
 */
public class User {


    private String pUserid, tweet;
    private int hashtagCount, urlCount, qouteOrMentionCount;

    public User(String pUserid, String tweet, int hashtagCount, int urlCount, int qouteOrMentionCount) {

    	this.pUserid = pUserid;
    	this.tweet = tweet;
    	this.hashtagCount = hashtagCount;
    	this.qouteOrMentionCount = qouteOrMentionCount;
    }

    public String getpUserid() {
        return pUserid;
    }

    public void setpUserid(String pUserid) {
        this.pUserid = pUserid;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public int getHashtagCount() {
        return hashtagCount;
    }

    public void setHashtagCount(int hashtagCount) {
        this.hashtagCount = hashtagCount;
    }

    public int getUrlCount() {
        return urlCount;
    }

    public void setUrlCount(int urlCount) {
        this.urlCount = urlCount;
    }

    public int getQouteOrMentionCount() {
        return qouteOrMentionCount;
    }

    public void setQouteOrMentionCount(int qouteOrMentionCount) {
        this.qouteOrMentionCount = qouteOrMentionCount;
    }


   
    
    
    
}
