/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import nlp.StanfordUtils;

/**
 *
 * @author Nandadeep
 * @description: scores the user profile based on extracted features
 */
public class Scoring {


    static ArrayList<String> privacyDict = new ArrayList<>();

	public Scoring(String dictPath) {

		//read privacy dictionary here
	}
    
    public static float urlScore(int urlCount) {
    	float score = 0;
        if(urlCount >= 3) {
            score = score + 10;
        } else if(urlCount == 0) {
            score = 0;
        } else if(urlCount <= 2) {
            score = score + 5;
        }
    	return score;
    }

    public static float hashtagScore(int hashtagCount) {
    	float score = 0;
        if(hashtagCount > 5) {
            score = score + 5;
        } else if(hashtagCount > 2 && hashtagCount < 5) {
            score = score + hashtagCount;
        } else {
            score = score + hashtagCount;
        }
    	return score;
    }

    public static float topicModelScore(String userId, ArrayList<String> topics) {
    	float score = 0;
        for(String topic : topics) {
            //for each topic out of the user tweet topics, increment score. 
            if(privacyDict.contains(topic)) {
                score = score + 2;
            }
        }
    	return score;
    }

    public static float entityScore(String tweet) {
        float score = 0;
        String model = "/home/hadoop/NetBeansProjects/Privacy/stanford-ner-2014-01-04/classifiers/english.conll.4class.distsim.crf.ser.gz";
        LinkedHashMap<String, ArrayList<String>> output = StanfordUtils.identifyNER(tweet, model);
        for(Map.Entry<String,ArrayList<String>> entry : output.entrySet()) {
            if(entry.getKey().equalsIgnoreCase("person")) {
                score = score + entry.getValue().size() + 2; //revealing person info = private af.
            } else if(entry.getKey().equalsIgnoreCase("location")) {
                score = score + entry.getValue().size(); // revealing location info = not that private af.
            } else {
                score = (float)(score + entry.getValue().size() *0.5); //revealing misc info is scored but not that important.
            }
        }
        return score;
    }


    
}
