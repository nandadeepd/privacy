/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author Nandadeep
 */
public class Tools{

    
    public static ArrayList<String> readFile(File f) throws FileNotFoundException, IOException {

    	ArrayList<String> content = new ArrayList<>();
    	BufferedReader br = new BufferedReader(new FileReader(f));
        String line = "";
        while((line = br.readLine()) != null) {
            content.add(line);
        }
    	return content;
    }

    public static ArrayList<String> getUserSpecificTopics(String userId, Map<String, ArrayList<String>> lda) {
        ArrayList<String> topics = new ArrayList<>();
        if(lda.containsKey(userId)) {
            topics = lda.get(userId);
        } 
        return topics;
    }

    public static void vectorJob(ArrayList<User> list, String output, Map<String, ArrayList<String>> lda) throws IOException {
        
        int qid = 1, pos = 1, neg = 2;
        for(User entry : list) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(output + entry.getpUserid() + ".dat"));
            ArrayList<String> topics = getUserSpecificTopics(entry.getpUserid(), lda);
            //accumulate scores
            float urlScore = Scoring.urlScore(entry.getUrlCount());
            float hashtagScore = Scoring.hashtagScore(entry.getHashtagCount());
            float quoteOrMentionScore = Scoring.urlScore(entry.getQouteOrMentionCount());
            float topicScore = Scoring.topicModelScore(entry.getpUserid(), topics); 
            float nerScore = Scoring.entityScore(entry.getTweet());
            //write vector dat file.
            if(topicScore!=0) {
                //positive label signifies that this entry reveals private info
                bw.write(String.valueOf(pos) + " " + String.valueOf(qid) + " 1:" + topicScore + " 2:" + urlScore + " 3:" + hashtagScore + " 4:" + quoteOrMentionScore + " 5:"+nerScore);
                bw.newLine();
            } else {
                //neg label signifies this entry does not reveal private info
                bw.write(String.valueOf(neg) + " " + String.valueOf(qid) + " 1:" + topicScore + " 2:" + urlScore + " 3:" + hashtagScore + " 4:" + quoteOrMentionScore + " 5:" + nerScore);
                bw.newLine();
            }
        }
    }

    public static Map<String, ArrayList<String>> readHLDAOutput(String ldaOutputPath) {
        Map<String, ArrayList<String>> dict = new HashMap<>();
        return dict;
    }
    
}
