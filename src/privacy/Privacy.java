/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package privacy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import utils.Scoring;
import utils.Tools;
import utils.User;

/**
 *
 * @author Nandadeep
 */
public class Privacy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // Paths
        String input = "./Training Data.txt";
        String inputdir = "Users/Nandadeep/Downloads/train/";
        String ldaOutputPath = "";
        String dictPath = "";
        String output = "./output/";
        // Regular Expressions
        String regex = "^(\\d+)\\t?(\\d+)\\t(.*)\\t";
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.DOTALL);
        Pattern quoteP = Pattern.compile("\\\"(.*?)\\\"");
        Pattern cuser = Pattern.compile("\\d+\\t\\@(.*?)\\s", Pattern.MULTILINE);
        // Files
        File data = new File(input);
        File[] corpus = new File(inputdir).listFiles();
        // Resources
        ArrayList<String> content = Tools.readFile(data);
        ArrayList<User> list = new ArrayList<>();
        Map<String, ArrayList<String>> ldaOutput = Tools.readHLDAOutput(ldaOutputPath);
        Scoring s = new Scoring(dictPath); // reads the privacy dict. 
        //System.out.println(content);
        //feature extraction begins
        //for(File user : corpus) {}
        content.stream().map((sentence) -> pattern.matcher(sentence)).filter((m) -> (m.find())).forEachOrdered((m) -> {
            //main tweet
            int quoteCount = 0;
            int mCount = 1;
            int count = 0;
            Matcher cuserM = cuser.matcher(m.group(0));
            if (cuserM.find()) { //for finding child user id to see if he has mentioned someone
                mCount++;
                count += mCount;
            }
            Matcher q = quoteP.matcher(m.group(3));
            if (q.find()) { //for finding if user has quoted something
                quoteCount++;
                count += quoteCount;
            }
            list.add(new User(m.group(1), m.group(3), 0, 0, count)); //collect all user particular features.
        });
        Tools.vectorJob(list, output, ldaOutput);
    }

}
